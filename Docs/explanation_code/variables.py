# Hier haben wir eine Variable namens x und wir haben den Wert 10 rein gelegt
x = 10
# mit print geben wir diese Zahl wieder aus
print(x)

# Auch können wir Berechnungen auf den Variablen ausführen, wie z.B. Addition, Multiplikation usw.
# Versuche folgenden Code auszugeben - was passiert?
print(x+2)

# In Variablen können wir nicht nur Zahlen speichern, sondern auch Wörter! In Python heissen Wörter Strings.
# Führe folgenden Code aus - was wird ausgegeben?
hobby = "schwimmen"
print("Mein Hobby ist", hobby)

# Es ist auch wichtig zu wissen

# das ist kein gueltiger variablen name = "TestVariable"

