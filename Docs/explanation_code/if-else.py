
# Wir fragen den Nutzenden nach dem Alter
alter_string = input("Wie alt bist du?")
# Wir konvertieren den string input in eine Zahl
alter_zahl = int(alter_string)
# Wenn das Alter 1 oder 0 ist, ist die Person noch ein Baby
if alter_zahl <= 1:
     print("Du bist ein Baby.")
elif alter_zahl <= 3:
     print("Du bist ein Kleinkind.")
elif alter_zahl <= 9:
     print("Du bist ein Kind.")
# Wenn das Alter grösser 10, aber kleiner 18 ist, ist die Person ein Teenager
elif  10   <= alter_zahl < 18:
     print("Du bist ein Teenager.")
# Für alle Zahlen grösser 18 sagen wir, dass die Person erwachsen ist. Dafür nutzen wir das else.
else:
    print("Du bist erwachsen.")




