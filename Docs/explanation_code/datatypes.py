
# Strings schreibt mit Anführungszeichen "" oder ''. Beides ist möglich. Aber nicht vermischen! So geht " ' nicht.
# All diese sind Strings
name = "Marie"
alter = '15'
# hier haben wir ein Array, also eine Liste von Strings
hobbies = ['reiten', 'schwimmen', 'joggen']

# All diese hier sind Integers
alter = 15
winter_temperatur = -20
lieblingszahlen = [1, 42, 93423482 ]

# All diese hier sind Floats, beachte für die Kommastelle wird ein . und kein , benutzt
# In Python werden Kommas nur zur Trennung von Werten genutzt, wie z.B. im Array
koerper_temperatur: 36.8
wetter_temperatur: 7.2
weitsprung_rekord: 8.95

# All diese hier sind Booleans
ich_mag_programmieren = True
ich_mag_winter_mehr_als_sommer = False


# Beispiel:
alter = 12  # int
gewicht = 45.6  # float
name = "Max"  # str
ist_schueler = True  # bool

#Achtung! Das vermischen von verschiedenen Datentypen funkioniert nicht, sonst bekommst du eine Fehlermeldung!
# Was du machen kannst, wenn du z.B. aus dem String "15" => 15, also die Zahl haben möchtest,
# du kannst sie umwandeln! Beim Programmieren spricht man da von Konvertierung.

# definiere dein Alter als String
mein_alter_als_string= "15"
# mit int() konvertieren wir den String zu einem Integer, also einer Zahl
mein_alter_als_int= int(mein_alter_als_string)
# nun ist es zu einer Zahl konvertieren, d.h. jetzt kannst du jegliche arithmetische Operation ausführen,
# wir machen hier eine einfache Addition
mein_alter_in_fuenf_jahren = mein_alter_als_int + 5
# wir geben noch das Ergebnis aus
print("In fünf Jahren werde ich ", mein_alter_in_fuenf_jahren, "sein!")




