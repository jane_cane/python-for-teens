

for i in range(10):  # Schleife von 0 bis 9
    print("Hallo!")  # Diese Zeile ist eingerückt und gehört zur Schleife
print("Fertig!")


for i in range(5):  # Schleife von 0 bis 4
    print(i)  # Diese Zeile ist eingerückt und gehört zur Schleife
print("Fertig!")  # Diese Zeile ist nicht eingerückt und gehört nicht zur Schleife


# Eine Liste von Früchten
fruechte = ["Apfel", "Banane", "Kirsche"]

# Mit einer for-Schleife über die Früchte gehen
for frucht in fruechte:
    print("Ich esse einen " + frucht)

# Ausgabe:
# Ich esse einen Apfel
# Ich esse einen Banane
# Ich esse einen Kirsche

