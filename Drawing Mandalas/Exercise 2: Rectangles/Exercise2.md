### Aufgabe 2: Rechtecke
Erstelle eine neue und leere Datei mit Namen `rechtecke.py`.
1. Zu Beginn importieren wir die Bibliothek turtle:   
`import turtle`

2. Als nächstes instanziieren wir eine turtle mit Parameternamen `shelly`.
Unsere Schildkröte heisst also shelly.   
`shelly = turtle.Turtle()`

3. Definiere eine Liste mit Farben, in denen später die Rechtecke gezeichnet werden sollen:   
`colors = ['red', 'yellow', 'orange', 'blue', 'green', 'purple']`
4. Definiere `y_position=30` mit der Variable werden wir später unseren Stift umpositionieren können, damit wir eine neue Reihe anfangen können. 

5. Wir wollen 6 Rechtecke zeichnen, daher eine for-Schleife mit 6 Iterationen:   
`for m in range(6): `
6. Wir setzen die Farbe mit: 
`shelly.color(colors[m])`
7. Nun zeichnen wir ein Rechteck. Da es 4 Kanten hat und 90°, erstellen wir eine for-Schleife mit 4 Iterationen und bewegen
`shelly` um 20 Schritte vorwärts und mit einer Drehung von 90° nach links.
```py
for k in range(4):
  shelly.forward(20)
  shelly.left(90)
```
8. Nachdem wir ein Rechteck gezeichnet haben, wollen wir shelly vorwärts bewegen, damit wir die Rechtecke nicht auf der gleichen Stelle zeichnen.
Mit Hilfe von `pendown()` und `penup()`können wir shelly sagen wann sie zeichnen soll und wann nicht.
9. Am Ende noch ein `turtle.done()`, damit die Zeichnung auch offen bleibt.

Der Code sollte nun in etwa so aussehen, achte auf die richtigen Einrückungen:
```py
for m in range(6):
   shelly.color(colors[m])
   for k in range(4):
       shelly.pendown()
       shelly.forward(20)
       shelly.left(90)
       shelly.penup()
   shelly.forward(30)
turtle.done()   
```
10. Und wie sieht es aus?   
Drücke auf den ![img.png](../res/run.png) Button um deine Rechtecke zu sehen.
Falls du das Icon nicht siehst, mache ein Rechtsklick auf deine Python-Datei und du kannst `Run rechtecke` ausführen.  
![img.png](../res/rectangles_row.png)    
11. Jetzt wollen wir noch die Kästchen ausmalen! Dafür können wir `begin_fill()` und `end_fill()` verwenden.
Diese müssen wir vor und nach dem Zeichnen des Rechtecks aufrufen, also vor und nach der for-Schleife mit 4 Iterationen.
```py
for m in range(6):
    shelly.color(colors[m])
    shelly.begin_fill()
    for k in range(4):
        shelly.pendown()
        shelly.forward(20)
        shelly.left(90)
        shelly.penup()
    shelly.end_fill()
    shelly.forward(30)
turtle.done()
```
Et voilà!    
![fill_rectangles.gif](../res/fill_rectangles.gif)

Wenn du grössere Rechtecke zeichnen willst, dann veränder doch die Grössen in `shelly.forward()` und `shelly.left()`

Nun wollen wir mehrere Reihen solcher Rechtecke zeichnen! Dafür gibt es `shelly.goto(x,y)`, wo wir 
unserer Schildkröte mitgeben können, zu welcher x- und y-Koordinate sie gehen soll.
Jetzt werden wir die ganz oben definierte `y_position` verwenden.  
Jedes Mal wenn wir die 6 Rechtecke fertig gezeichnet haben, bewegen wir die Schildkröte zu  `shelly.goto(0, y_position)`
D.h. die x-Koordinate bleibt dieselbe, aber wir werden jedes Mal `y_position` um 30 erhöhen mit `y_position += 30`.

![rectangle_finish.png](../res/rectangle_finish.png)


Der finale Code sollte nun so aussehen:
<details>
  <summary>Klicken um Lösung anzuzeigen</summary>

```py
import turtle
shelly = turtle.Turtle()

colors = ['red', 'yellow', 'orange', 'blue', 'green', 'purple']
y_position = 30
for n in range(6):
    for m in range(6):
        shelly.color(colors[m])
        shelly.begin_fill()
        for k in range(4):
            shelly.pendown()
            shelly.forward(20)
            shelly.left(90)
            shelly.penup()
        shelly.end_fill()
        shelly.forward(30)
    shelly.goto(0,y_position)
    y_position += 30
turtle.done()
```
</details>

Führe den Code nochmals aus und schaue wie es aussieht, das kann ein Weilchen dauern. 

Wenn du mit dieser Aufgabe fertig bist und genügend ausprobiert hast, kannst du hier zur nächsten: [Aufgabe 3: Kreis Mandala](/Drawing%20Mandalas/Exercise%203:%20Circle%20Mandala/Exercise3.md)
