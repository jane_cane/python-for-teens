import turtle
shelly = turtle.Turtle()
colors = ['red', 'yellow', 'purple', 'blue', 'green', 'orange']
for i in range(36):
    for i in range(6):
        shelly.color(colors[i])
        shelly.forward(100)
        shelly.left(60)
    shelly.right(10)
turtle.done()
