import turtle
shelly = turtle.Turtle()

colors = ['red', 'yellow', 'orange', 'blue', 'green', 'purple']
kreis_groesse =  20

for m in range(36):
    shelly.penup()
    shelly.forward(200)
    for i in range(6):
        shelly.pensize(10)
        shelly.color(colors[i])
        shelly.begin_fill()
        shelly.pendown()
        shelly.circle(kreis_groesse)
        shelly.penup()
        shelly.back(20)
        shelly.end_fill()
        kreis_groesse -= 2
    kreis_groesse = 20
    shelly.back(80)
    shelly.right(20)
turtle.done()
