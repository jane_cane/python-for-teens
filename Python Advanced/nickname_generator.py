from pprint import pprint

from german_nouns.lookup import Nouns
from german_nouns.lookup import Nouns


# Initialize the Nouns class

nouns = Nouns()


# Lookup a word

word = nouns['Haus']

pprint(word)


# Parse a compound word

words = nouns.parse_compound('dumm')

print(words)