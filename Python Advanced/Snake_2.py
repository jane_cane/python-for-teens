import pygame
import random

# Initialize Pygame
pygame.init()

# Game constants
WIDTH, HEIGHT = 640, 480  # Set the width and height of the game window
BLOCK_SIZE = 20  # Set the size of each block in the game (snake body, food, etc.)
SPEED = 10  # Set the speed of the game (higher values make the game faster)

# Colors
BLACK = (0, 0, 0)  # Set the background color to black
WHITE = (255, 255, 255)  # Set the color of the snake body to white
RED = (255, 0, 0)  # Set the color of the food to red

# Initialize game window
screen = pygame.display.set_mode((WIDTH, HEIGHT))  # Create a game window with the specified width and height
pygame.display.set_caption("Snake Game")  # Set the title of the game window

# Snake class
class Snake:
    def __init__(self):
        # Initialize the snake's body as a list of coordinates
        self.body = [(WIDTH // 2, HEIGHT // 2)]  # Start the snake in the middle of the screen
        # Initialize the snake's direction (right, left, up, or down)
        self.direction = (1, 0)  # Start the snake moving to the right

    def move(self):
        # Move the snake's head in the current direction
        head = self.body[0]
        new_head = (head[0] + self.direction[0] * BLOCK_SIZE, head[1] + self.direction[1] * BLOCK_SIZE)
        self.body.insert(0, new_head)  # Add the new head to the front of the snake's body

    def eat(self, food):
        # Check if the snake's head has collided with the food
        if self.body[0] == food:
            return True  # If so, return True to indicate that the snake has eaten
        else:
            self.body.pop()  # If not, remove the last segment of the snake's body
            return False

    def draw(self):
        # Draw the snake's body on the screen
        for pos in self.body:
            pygame.draw.rect(screen, WHITE, (pos[0], pos[1], BLOCK_SIZE, BLOCK_SIZE))

# Food class
class Food:
    def __init__(self):
        # Generate a random position for the food
        self.pos = (random.randint(0, WIDTH - BLOCK_SIZE) // BLOCK_SIZE * BLOCK_SIZE, random.randint(0, HEIGHT - BLOCK_SIZE) // BLOCK_SIZE * BLOCK_SIZE)

    def draw(self):
        # Draw the food on the screen
        pygame.draw.rect(screen, RED, (self.pos[0], self.pos[1], BLOCK_SIZE, BLOCK_SIZE))

# Game loop
snake = Snake()  # Create a new snake object
food = Food()  # Create a new food object
clock = pygame.time.Clock()  # Create a clock object to control the game speed

running = True
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False  # If the user closes the game window, stop the game loop
        elif event.type == pygame.KEYDOWN:
            # Handle user input (arrow keys)
            if event.key == pygame.K_UP and snake.direction != (0, 1):
                snake.direction = (0, -1)  # Move up
            elif event.key == pygame.K_DOWN and snake.direction != (0, -1):
                snake.direction = (0, 1)  # Move down
            elif event.key == pygame.K_LEFT and snake.direction != (1, 0):
                snake.direction = (-1, 0)  # Move left
            elif event.key == pygame.K_RIGHT and snake.direction != (-1, 0):
                snake.direction = (1, 0)  # Move right

    snake.move()  # Move the snake
    if snake.eat(food.pos):
        food = Food()  # If the snake eats the food, generate a new food object
    else:
        # Check for collisions with the edge of the screen or the snake's own body
        if (snake.body[0][0] < 0 or snake.body[0][0] >= WIDTH or
            snake.body[0][1] < 0 or snake.body[0][1] >= HEIGHT or
            snake.body[0] in snake.body[1:]):
            running = False  # If a collision is detected, stop the game loop

    screen.fill(BLACK)  # Clear the screen
    snake.draw()  # Draw the snake
    food.draw()  # Draw the food
    pygame.display.flip()
