### Einrückungen
- Janet 

### Funktionen 
- Yelin 

### Datentypen 
- Yelin 

### Vergleichsoperatoren 
- Samuel 

### Quiz erstellen 

 # Python Basics Quiz

## Frage 1
Was macht der Befehl `print("Hallo, Welt!")` in Python?

- a) Er zeigt den Text "Hallo, Welt!" auf dem Bildschirm an.
- b) Er speichert den Text "Hallo, Welt!" in einer Datei.
- c) Er löscht den Text "Hallo, Welt!" vom Bildschirm.

**Richtige Antwort:** a

---

## Frage 2
Wie wird eine Variable in Python erstellt?

- a) `var x = 10`
- b) `x = 10`
- c) `create x 10`

**Richtige Antwort:** b

---

## Frage 3
Welche dieser Zahlen ist eine `float` Zahl?

- a) 10
- b) 10.5
- c) "10"

**Richtige Antwort:** b

---

## Frage 4
Was gibt der folgende Code aus? `x = 5; print(x * 2)`

- a) 5
- b) 10
- c) 25

**Richtige Antwort:** b

---

## Frage 5
Welche der folgenden Funktionen wird verwendet, um den Datentyp einer Variable zu überprüfen?

- a) `type()`
- b) `check()`
- c) `datatype()`

**Richtige Antwort:** a

---

## Frage 6
Was passiert, wenn du versuchst, zwei Strings zusammenzufügen? (z.B. `"Hallo" + " Welt"`)

- a) Die beiden Strings werden multipliziert.
- b) Die beiden Strings werden addiert und ergeben einen neuen String.
- c) Ein Fehler wird angezeigt.

**Richtige Antwort:** b

---

## Frage 7
Welche der folgenden Schleifen gibt es in Python?

- a) `for`-Schleife
- b) `while`-Schleife
- c) Beide

**Richtige Antwort:** c

---

## Frage 8
Was ist der richtige Weg, eine `if`-Bedingung in Python zu schreiben?

- a) `if x = 5`
- b) `if x == 5`
- c) `if 5 == x`

**Richtige Antwort:** b

---

## Frage 9
Wie kannst du die Eingabe eines Benutzers in Python speichern?

- a) `input()`
- b) `print()`
- c) `read()`

**Richtige Antwort:** a

---

## Frage 10
Wie kannst du eine Liste in Python definieren?

- a) `list = (1, 2, 3)`
- b) `list = [1, 2, 3]`
- c) `list = {1, 2, 3}`

**Richtige Antwort:** b

---

## Frage 11
Welche dieser Optionen ist keine Datenstruktur in Python?

- a) Liste
- b) Set
- c) Tabelle

**Richtige Antwort:** c

---

## Frage 12
Wie kannst du in Python ein Kommentar hinzufügen?

- a) `// Dies ist ein Kommentar`
- b) `# Dies ist ein Kommentar`
- c) `/* Dies ist ein Kommentar */`

**Richtige Antwort:** b

---

## Frage 13
Welches der folgenden Symbole wird verwendet, um einen Block Code in Python zu kennzeichnen?

- a) Klammern `{ }`
- b) Doppelpunkte `:`
- c) Semikolon `;`

**Richtige Antwort:** b

---

## Frage 14
Was passiert, wenn du versuchst, auf einen Index außerhalb der Liste zuzugreifen?

- a) Es wird der letzte Wert der Liste angezeigt.
- b) Ein Fehler wird angezeigt.
- c) Die Liste wird leer.

**Richtige Antwort:** b

---

## Frage 15
Was gibt die Funktion `len()` in Python zurück?

- a) Die Länge eines Strings
- b) Die Größe einer Liste
- c) Beide Antworten sind richtig

**Richtige Antwort:** c

---

## Frage 16
Welche der folgenden Optionen ist eine richtige `while`-Schleife?

- a) `while x < 10:`
- b) `while (x < 10)`
- c) `while x < 10 do`

**Richtige Antwort:** a

---

## Frage 17
Welche Funktion wird verwendet, um einen Wert in eine andere Datentyp zu konvertieren?

- a) `convert()`
- b) `change()`
- c) `int()`

**Richtige Antwort:** c

---

## Frage 18
Wie sieht der richtige Code aus, um eine Funktion zu definieren?

- a) `def meineFunktion():`
- b) `function meineFunktion():`
- c) `create meineFunktion():`

**Richtige Antwort:** a

---

## Frage 19
Was passiert, wenn du eine Variable nicht definierst, bevor du sie verwendest?

- a) Es wird ein Standardwert zugewiesen.
- b) Ein Fehler wird angezeigt.
- c) Die Variable wird automatisch als null gesetzt.

**Richtige Antwort:** b

---

## Frage 20
Wie kannst du den Wert einer Variablen in Python erhöhen?

- a) `x = x + 1`
- b) `x = x - 1`
- c) `x = x * 2`

**Richtige Antwort:** a

---
 

### Brücke Scratch Python
 -  Janet 

### Begriffe 
 -  alle

 # Python Basics

## 1. *Variable*
Eine *Variable* ist wie ein Schrank, in dem du Dinge aufbewahrst. Der Schrank hat einen Namen, und du kannst jederzeit etwas hineinlegen oder herausnehmen.

Beispiel:
x = 10

Hier haben wir eine Variable namens x, und wir haben den Wert 10 in diese Variable gelegt.

## 2. *Datentypen*
Ein *Datentyp* gibt an, welche Art von Information in einer Variable gespeichert werden kann. Die wichtigsten Datentypen in Python sind:

- *int*: Ganze Zahlen, z.B. 10 oder -5.
- *float*: Dezimalzahlen, z.B. 3.14 oder -2.5.
- *str*: Zeichenketten (Text), z.B. "Hallo" oder "Python".
- *bool*: Wahrheitswerte, entweder True (wahr) oder False (falsch).

Beispiel:
alter = 12  # int  
gewicht = 45.6  # float  
name = "Max"  # str  
ist_schueler = True  # bool

## 3. *Operatoren*
*Operatoren* sind Symbole, mit denen du Berechnungen anstellst oder zwei Werte miteinander vergleichst. Es gibt viele Arten von Operatoren:

- *Arithmetische Operatoren*: +, -, *, /, //, %, ** (Addition, Subtraktion, Multiplikation, Division, Ganzzahl-Division, Rest, Potenz).
- *Vergleichsoperatoren*: ==, !=, >, <, >=, <= (Vergleich von Werten).

Beispiel:
a = 10  
b = 5  
ergebnis = a + b  # Ergebnis ist 15

## 4. *Bedingte Anweisungen (If-Else)*
Mit *if-else* kannst du Entscheidungen treffen. Du prüfst, ob etwas wahr oder falsch ist, und führst dann bestimmte Aktionen aus.

Beispiel:
alter = 12  
if alter >= 18:  
&nbsp;&nbsp;&nbsp;&nbsp;print("Du bist erwachsen.")  
else:  
&nbsp;&nbsp;&nbsp;&nbsp;print("Du bist noch ein Kind.")

## 5. *Schleifen*
*Schleifen* helfen dir, etwas mehrmals zu tun. Es gibt zwei Haupttypen:

- *for-Schleife*: Wird verwendet, wenn du genau weißt, wie oft du etwas tun willst.
- *while-Schleife*: Wiederholt sich, solange eine Bedingung wahr ist.

Beispiel für eine for-Schleife:
for i in range(5):  
&nbsp;&nbsp;&nbsp;&nbsp;print(i)  
Das gibt dir die Zahlen 0 bis 4.

## 6. *Funktionen*
Eine *Funktion* ist wie eine kleine Maschine: Du gibst ihr etwas, und sie gibt dir etwas zurück. Du kannst sie immer wieder verwenden, ohne den Code jedes Mal neu zu schreiben.

Beispiel:
def addiere(x, y):  
&nbsp;&nbsp;&nbsp;&nbsp;return x + y  

ergebnis = addiere(5, 10)  # Gibt 15 zurück

## 7. *Listen*
Eine *Liste* ist wie eine Sammlung von Dingen. Du kannst sie mit eckigen Klammern [] erstellen, und sie kann viele verschiedene Werte speichern.

Beispiel:
fruits = ["Apfel", "Banane", "Kirsche"]

Du kannst auf die einzelnen Elemente zugreifen, indem du den Index angibst (z.B. fruits[0] für "Apfel").

## 8. *Dictionaries (Wörterbuch)*
Ein *Dictionary* ist wie ein echtes Wörterbuch, aber statt Wörter und Definitionen speichert es *Schlüssel* und *Werte*.

Beispiel:
person = {"Name": "Max", "Alter": 12}  
Hier ist "Name" der Schlüssel und "Max" der Wert.

## 9. *Klassen und Objekte*
Eine *Klasse* ist ein Bauplan für ein Objekt. Ein *Objekt* ist eine Instanz dieser Klasse. Mit Klassen kannst du eigene Dinge, wie z.B. einen Hund oder ein Auto, erstellen.

Beispiel:
class Hund:  
&nbsp;&nbsp;&nbsp;&nbsp;def __init__(self, name, alter):  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;self.name = name  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;self.alter = alter  

def bellen(self):  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;print(f"{self.name} bellt!")  

hund1 = Hund("Rex", 5)  
hund1.bellen()  # "Rex bellt!"

## 10. *Importieren von Modulen*
Ein *Modul* ist eine Sammlung von Funktionen und Code, die du in deinem Programm verwenden kannst. Mit import kannst du ein Modul in deinem Programm einbinden.

Beispiel:
import math  
print(math.sqrt(16))  # Gibt 4.0 zurück

## 11. *Fehlerbehandlung (Exceptions)*
Mit *Fehlerbehandlung* kannst du dein Programm vor Abstürzen schützen. Wenn ein Fehler auftritt, kannst du ihm sagen, was zu tun ist.

Beispiel:
try:  
&nbsp;&nbsp;&nbsp;&nbsp;x = 10 / 0  
except ZeroDivisionError:  
&nbsp;&nbsp;&nbsp;&nbsp;print("Fehler: Du kannst nicht durch Null teilen!")

## 12. *Kommentare*
Ein *Kommentar* ist ein Teil des Codes, der nicht ausgeführt wird. Du kannst ihn verwenden, um deinen Code zu erklären.

Beispiel:
# Das ist ein Kommentar  
x = 10  # Dies ist eine Variable

## 13. *List Comprehensions*
Mit *List Comprehensions* kannst du Listen auf einfache Weise erstellen. Sie sind eine verkürzte Form von Schleifen.

Beispiel:
zahlen = [x**2 for x in range(5)]  # Erstellt eine Liste der Quadrate der Zahlen 0 bis 4

## 14. *Lambda-Funktionen*
Eine *Lambda-Funktion* ist eine kleine, anonyme Funktion, die in einer einzigen Zeile definiert wird.

Beispiel:
quadrieren = lambda x: x**2  
print(quadrieren(4))  # Gibt 16 zurück

## 15. *Pass-Statement*
Das *pass*-Statement wird verwendet, wenn du einen Platzhalter für einen Codeblock brauchst, aber noch keinen Code einfügen möchtest.

Beispiel:
if True:  
&nbsp;&nbsp;&nbsp;&nbsp;pass  # Nichts tun

## 16. *Range-Funktion*
Mit *range()* kannst du eine Sequenz von Zahlen erstellen, die oft in Schleifen verwendet wird.

Beispiel:
for i in range(3):  
&nbsp;&nbsp;&nbsp;&nbsp;print(i)  
Das gibt dir die Zahlen 0, 1, 2.

## 17. *Indexierung und Slicing*
Mit *Indexierung* kannst du auf einzelne Elemente in einer Liste oder einem String zugreifen. Mit *Slicing* kannst du Teile einer Liste oder eines Strings extrahieren.

Beispiel:
text = "Hallo"  
print(text[0])  # Gibt 'H' zurück  
print(text[1:4])  # Gibt 'all' zurück

## 18. *File I/O (Dateien lesen und schreiben)*
Mit *File I/O* kannst du Dateien auf deinem Computer öffnen, lesen oder schreiben.

Beispiel:
# Schreiben in eine Datei  
with open("beispiel.txt", "w") as f:  
&nbsp;&nbsp;&nbsp;&nbsp;f.write("Hallo, Welt!")

# Lesen aus einer Datei  
with open("beispiel.txt", "r") as f:  
&nbsp;&nbsp;&nbsp;&nbsp;inhalt = f.read()  
&nbsp;&nbsp;&nbsp;&nbsp;print(inhalt)  # Gibt "Hallo, Welt!" aus

## 19. *Set (Menge)*
Ein *Set* ist eine Sammlung von einzigartigen Elementen, die keine Reihenfolge haben.

Beispiel:
farben = {"rot", "grün", "blau"}  
farben.add("gelb")  # Fügt "gelb" hinzu

## 20. *Argumente und Parameter*
Ein *Argument* ist ein Wert, den du einer Funktion übergibst. Ein *Parameter* ist der Platzhalter in der Funktion, der das Argument aufnimmt.

Beispiel:
def begruessung(name):  
&nbsp;&nbsp;&nbsp;&nbsp;print(f"Hallo, {name}!")

begruessung("Max")  # "Max" ist das Argument

## 21. *Globale und lokale Variablen*
*Globale Variablen* existieren überall im Code, während *lokale Variablen* nur innerhalb einer Funktion existieren.

Beispiel:
x = 10  # Globale Variable

def meine_funktion():  
&nbsp;&nbsp;&nbsp;&nbsp;y = 5  # Lokale Variable  
&nbsp;&nbsp;&nbsp;&nbsp;print(x + y)

meine_funktion()  # Gibt 15 aus

## 22. *Decorator*
Ein *Decorator* ist eine Funktion, die eine andere Funktion verändert oder erweitert.

Beispiel:
def mein_decorator(funktion):  
&nbsp;&nbsp;&nbsp;&nbsp;def wrapper():  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;print("Vor der Funktion")  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;funktion()  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;print("Nach der Funktion")  
&nbsp;&nbsp;&nbsp;&nbsp;return wrapper

@mein_decorator  
def hallo():  
&nbsp;&nbsp;&nbsp;&nbsp;print("Hallo!")

hallo()  

## 23. *Typecasting*
*Typecasting* bedeutet, den Datentyp einer Variablen zu ändern.

Beispiel:
x = "10"  
y = int(x)  # Wandelt "10" von einem String in eine Zahl  
print(y + 5)  # Gibt 15 aus

## 24. *Enumerate*
Mit *enumerate()* kannst du sowohl den Index als auch das Element einer Liste bekommen.

Beispiel:
farben = ["rot", "grün", "blau"]  
for index, farbe in enumerate(farben):  
&nbsp;&nbsp;&nbsp;&nbsp;print(index, farbe)

## 25. *Zip*
Die *zip()*-Funktion kombiniert mehrere Listen in eine Liste von Tupeln.

Beispiel:
namen = ["Alice", "Bob"]  
alter = [25, 30]  
kombiniert = zip(namen, alter)  
print(list(kombiniert))  # Gibt [('Alice', 25), ('Bob', 30)] zurück

## 26. *All und Any*
Mit *all()* prüfst du, ob alle Bedingungen wahr sind, und mit *any()* prüfst du, ob wenigstens eine wahr ist.

Beispiel:
zahlen = [1, 2, 3]  
print(all(zahlen))  # Gibt True zurück  
print(any(zahlen))  # Gibt True zurück

## 27. *assert*
Das *assert*-Statement wird verwendet, um eine Bedingung zu überprüfen. Wenn sie falsch ist, wird ein Fehler ausgelöst.

Beispiel:
x = 10  
assert x > 0  # Keine Ausnahme  
assert x < 0  # Löst eine Ausnahme aus

## 28. *Komma in Ausdruck (Multiple Werte)*
In Python kannst du mehreren Variablen gleichzeitig Werte zuweisen.

Beispiel:
x, y, z = 1, 2, 3

## 29. **Schlüsselwort del**
Mit *del* kannst du Variablen oder Elemente aus Listen löschen.

Beispiel:
x = 10  
del x  # Löscht die Variable 'x'

## 30. *Ternärer Operator*
Der *ternäre Operator* ist eine kurze Möglichkeit, eine If-Else-Bedingung in einer einzigen Zeile zu schreiben.

Beispiel:
x = 5  
result = "größer als 3" if x > 3 else "kleiner oder gleich 3"

## 31. *Dokumentationsstring*
Ein *Dokumentationsstring (docstring)* wird verwendet, um Funktionen oder Module zu beschreiben.

Beispiel:
def meine_funktion():  
&nbsp;&nbsp;&nbsp;&nbsp;"""Dies ist eine Funktion, die nichts tut."""  
&nbsp;&nbsp;&nbsp;&nbsp;pass

## 32. *Methoden*
*Methoden* sind Funktionen, die an ein Objekt gebunden sind.

Beispiel:
text = "Python"  
print(text.upper())  # Gibt "PYTHON" zurück

## 33. *isinstance*
Mit *isinstance()* überprüfst du, ob ein Objekt von einem bestimmten Datentyp ist.

Beispiel:
x = 10  
print(isinstance(x, int))  # Gibt True zurück

## 34. *Komplexe Zahlen*
*Komplexe Zahlen* bestehen aus einem realen und einem imaginären Teil.

Beispiel:
z = 3 + 4j  

## 35. *Funktionsüberladung*
Python unterstützt keine klassische *Funktionsüberladung*, aber du kannst Argumente optional machen.

Beispiel:
def hallo(name="Gast"):  
&nbsp;&nbsp;&nbsp;&nbsp;print(f"Hallo, {name}!")

## 36. *Global-Keyword*
Das *global*-Keyword wird verwendet, um auf eine globale Variable in einer Funktion zuzugreifen.

Beispiel:
x = 5

def aendere_x():  
&nbsp;&nbsp;&nbsp;&nbsp;global x  
&nbsp;&nbsp;&nbsp;&nbsp;x = 10

## 37. *Weak References*
*Weak References* sind Referenzen auf Objekte, die den Zähler für die Referenzen nicht erhöhen.

## 38. *Super*
Die *super()*-Funktion wird verwendet, um auf die Methode der Basisklasse zuzugreifen.
