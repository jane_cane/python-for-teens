from wiktionaryparser import WiktionaryParser

# Create a parser instance
parser = WiktionaryParser()

# Define the word you want to look up
word = "example"

# Fetch the word definitions
word_data = parser.fetch(word)

# Print the meanings
if word_data:
    print(f"Meanings of '{word}':")
    for entry in word_data:
        for definition in entry['definitions']:
            print(f"{definition['partOfSpeech']}:")
            for meaning in definition['text']:
                print(f" - {meaning}")
else:
    print(f"No meaning found for '{word}'.")