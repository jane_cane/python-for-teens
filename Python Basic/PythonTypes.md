# Datentypen

Um Daten wie Text, Zahlen und mehr dem Computer verständlich zu machen, gibt es **Datentypen**. Sie helfen deinem Code, zu wissen, wie die gegebenen Daten zu behandeln sind.  

## Was sind Datentypen?  
Die verschiedenen Datentypen in Python erlauben es dir, unterschiedliche Arten von Informationen darzustellen.  


### Texte (String)
Texte werden mit Gänsefüßchen (`'` oder `"`) definiert:  
```python
"Apfel"  
'Birne'
```  
Wichtig: Am Anfang und Ende des Textes müssen die Gänsefüßchen gleich sein, sonst gibt es einen Fehler:  
```
"Bern'  # Fehler!
'Basel"  # Fehler!
```  

Strings (Texte) können Buchstaben, Zahlen und sogar Symbole enthalten.  


### Zahlen (Integer / Float) 
Python unterscheidet zwischen **ganzen Zahlen** und **Kommazahlen**:  

- **Integer** (Ganze Zahlen):  
```python
1  
42  
-10
```  

- **Float** (Kommazahlen):  
```python
1.5  
3.14  
-0.01
```  
Kommazahlen werden so genannt, weil sie einen Punkt als Trennzeichen zwischen dem ganzzahligen und dem Dezimalteil verwenden.  

---

### **Wahr/Falsch (Boolean)**  
Python hat auch einen Datentyp für **wahr** und **falsch**. Damit kannst du Fakten und Entscheidungen darstellen:  
```python
ist_rot = True  
ist_blau = False
```  
Der **Boolean-Typ** ist sehr nützlich für Vergleiche und Bedingungen (`if`).  

---

### Kein Wert (None)
Manchmal möchtest du ausdrücken, dass etwas keinen Wert hat. Dafür gibt es den speziellen Datentyp `None`:  
```python
wert = None  
```  
Das bedeutet, dass `wert` aktuell leer ist.  


### Listen und Zuordnungen
Manchmal brauchst du Strukturen, um mehrere Werte zu speichern:  

#### Liste (Array / List)
Eine Liste speichert mehrere Werte in einer bestimmten Reihenfolge:  
```python
fruechte = ["Apfel", "Birne", "Kirsche"]
```
Auf die einzelnen Elemente kannst du mit ihrem Index zugreifen. Dabei beginnt der Index bei 0:
```python
print(fruechte[0])  # "Apfel"  
print(fruechte[1])  # "Birne"  
print(fruechte[2])  # "Kirsche"  
```
Du kannst auch die Elemente der Liste ändern:

```python
fruechte[1] = "Banane"  
print(fruechte)  # ["Apfel", "Banane", "Kirsche"]
```

#### Zuordnungen (Dictionary / Map) 
Ein Wörterbuch speichert Zuordnungen von Schlüsseln zu Werten:  
```python
lebensmittel = {"Apfel": "Frucht", "Birne": "Frucht", "Lachs": "Fisch"}
```

Auf die Werte kannst du zugreifen, indem du den Schlüssel angibst:

```python
print(lebensmittel["Apfel"])  # "Frucht"  
print(lebensmittel["Lachs"])  # "Fisch"  
```
Du kannst auch neue Schlüssel-Wert-Paare hinzufügen oder bestehende Werte ändern:

```python
lebensmittel["Karotte"] = "Gemüse"  
print(lebensmittel)  # {"Apfel": "Frucht", "Lachs": "Fisch", "Karotte": "Gemüse"}  

lebensmittel["Apfel"] = "Snack"  
print(lebensmittel["Apfel"])  # "Snack"
```

## Mit Datentypen arbeiten


### Datentypen herausfinden
Du kannst den Datentyp eines Wertes mit der `type`-Funktion herausfinden:  
```python
print(type(42))       # <class 'int'>  
print(type(3.14))     # <class 'float'>  
print(type("Apfel"))  # <class 'str'>  
```  

## Exkurse

### Falsche Datentypen verwenden
Wenn du nicht den richtigen Datentyp verwendest, führt das zu Fehlern.

10 + 10 = 20 oder nicht?
```python
zahl1 = "10"
zahl2 = 10  
summe = zahl1 + zahl2  # Fehler!
```
Python versteht nicht, wie es einen String `zahl1` (`"10"`) mit einem Integer `zahl2` (`10`) kombinieren soll. Du musst also sicherstellen, dass beide Werte denselben Datentyp haben.

Damit es wie erwartet funktioniert muss der Typ von `zahl1` auf Integer oder Float geaendert werden.
```python
zahl1 = "10"
zahl2 = 10
zahl1 = int(zahl1)
summe = zahl1 + zahl2  
print(summe)  # 20
```

### Vergleich zu anderen Programmiersprachen  
Python wird oft als eine **dynamisch typisierte** Programmiersprache bezeichnet. Das bedeutet, dass du nicht explizit angeben musst, welchen Datentyp eine Variable hat – Python erkennt dies automatisch:  
```python
zahl = 42       # Python weiss, dass dies ein Integer ist  
text = "Hallo"  # Python weiss, dass dies ein String ist  
```  
In anderen Programmiersprachen, wie z. B. Java, musst du die Typen immer vorher festlegen:  
```java
int zahl = 42;  
String text = "Hallo";  
```  

Python macht es dir dadurch einfacher, schnell loszulegen!  

