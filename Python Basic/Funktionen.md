# Funktionen  

Funktionen sind ein abstraktes Konzept, das in fast jeder Programmiersprache existiert.  

## Eine einfache Analogie  
Man kann sich eine Funktion wie ein Rezept vorstellen:  
- **Programmiersprache**: Der Koch, der das Rezept ausführt.  
- **Funktion**: Das Rezept selbst, das bestimmte Schritte vorschreibt.  
- **Parameter**: Die Zutaten, die das Rezept verlangt.  
- **Rückgabewert**: Das fertige Gericht, das am Ende entsteht.  

Allerdings sollte man diese Analogie nicht überstrapazieren, da Funktionen in der Programmierung viel vielseitiger sind.  

Jede Programmiersprache hat vorgefertigte Funktionen, die für häufige Aufgaben genutzt werden können und ermöglicht eigene Funktionen zu definieren.  

### Funktion  
Die `input`-Funktion wird verwendet, um eine Texteingabe vom Benutzer zu erhalten:  
```python
benutzername = input("Wie heißt du? ")
print("Hallo, " + benutzername + "!")
```  
Hier wird die Funktion `input()` und `print()` aufgerufen. Die Klammern hinter dem Namen einer Funktion bedeuten, dass die Funktion ausgeführt wird. Dies gilt für alle Funktionen. Um sie zu gebrauchen muss der Funktionsname gefolgt von Klammern sein.

Um Funktionen selber zu definieren muss man folgendes Schema befolgen:
```python
def funktionsname(parameter1, parameter2):  
    # Funktionskörper: Hier steht der Code, den die Funktion ausführt
    return "rückgabewert"
```

1. **Schlüsselwort `def`**  
   - Mit `def` teilst du Python mit, dass du eine Funktion definierst.  

2. **Funktionsname**  
   - Der Name der Funktion sollte beschreiben, was die Funktion tut.  
   - Zum Beispiel: `addiere`, `begrüße`, `multipliziere`.  
   - Der Name sollte keine Leerzeichen enthalten (stattdessen kannst du `_` verwenden).  

3. **Parameter**  
   - Parameter stehen in runden Klammern `()`.  
   - Sie sind Platzhalter für Werte, die der Funktion übergeben werden.  
   - Eine Funktion kann auch ohne Parameter definiert werden.  

4. **Doppelpunkt**  
   - Nach dem Funktionskopf (der Zeile mit `def`) steht immer ein Doppelpunkt `:`.  

5. **Funktionskörper**  
   - Der Funktionskörper enthält den Code, den die Funktion ausführt.  
   - Dieser Code muss nach dem `def` **eingerückt** sein (normalerweise 4 Leerzeichen).  

6. **Rückgabewert**  
   - Die Funktion kann mit `return` einen Wert zurückgeben.  
   - Wenn kein Rückgabewert benötigt wird, kannst du `return` weglassen. 

### Rückgabewerte  
Eine Funktion kann einen Wert zurückgeben, der mit dem Schlüsselwort `return` definiert wird:  
```python
def addiere(a, b):
    return a + b

ergebnis = addiere(3, 4)
print(ergebnis)  # 7
```  
Der Rückgabewert (`a + b`) wird an die Stelle zurückgegeben, wo die Funktion aufgerufen wurde.  


#### Funktionen ohne Rückgabewert
Funktionen müssen keinen Rückgabewert haben. Sie können stattdessen einfach eine Aktion ausführen:  
```python
def begruessen(name):
    print("Hallo, " + name + "!")

begruessen("Anna")  # "Hallo, Anna!"
begruessen("Max")  # "Hallo, Max!"
```

### Parameter  
Parameter sind Daten, die der Funktion übergeben werden. Sie können unterschiedlichste Informationen enthalten:  
- Daten, die verarbeitet werden sollen.  
- Einstellungen, die das Verhalten der Funktion steuern.  

Beispiel mit Parametern:  
```python
def multipliziere(a, b):
    return a * b

print(multipliziere(5, 3))  # 15
```  

Argumente werden oft synonym zu Parameter gebraucht.

### Funktionen und Methoden  
Der Begriff "Methode" wird oft synonym zu "Funktion" verwendet. Streng genommen ist es nicht ganz dasselbe, doch dies spielt am Anfang keine Rolle.  

### Warum Funktionen nutzen?  

1. **Vermeidung von doppeltem Code**:  
   Funktionen verhindern, dass du denselben Code an mehreren Stellen wiederholst. Indem die dieselbe Funktion an mehreren stellen gebraucht werden kann.

2. **Code vereinfachen und strukturieren**:  
   Komplizierter Code kann durch das Extrahieren von Teilen in Funktionen  verständlicher gemacht werden.  

3. **Flexibilität**:  
   Funktionen können für verschiedene Eingaben oder Szenarien angepasst werden durch Parameter.

### Beispiele von Funktionen in Einsatz

#### Eingabe-Validierung  
Eine Funktion kann Eingaben überprüfen und sicherstellen, dass sie korrekt sind:  
```python
def akzeptieren():
    eingabe = input("Stimmst du zu?")
    return eingabe == "ja"

print("Nimmst du die AGB an?")
antwort = akzeptieren()
if antwort:
    print("Du stimmst zu!")
else:
    print("Du lehnst ab!")
```  

#### Rekursion:  
Eine Funktion kann sich selbst aufrufen, um ein Problem schrittweise zu lösen:  
```python
def fakultaet(n):
    if n == 0:
        return 1
    return n * fakultaet(n - 1)

print(fakultaet(5))  # 120
```

#### Funktionen als Parameter:  
Funktionen können anderen Funktionen übergeben werden:  
```python
def anwenden(funktion, wert):
    return funktion(wert)

print(anwenden(abs, -5))  # 5
```  

#### Lambda-Funktionen:  
Lambda-Funktionen sind kleine Funktionen, die in einer Zeile definiert werden können:  
```python
quadrat = lambda x: x * x
print(quadrat(4))  # 16
```  

