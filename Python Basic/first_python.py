# Coding Beispiel : Print
print("Hello Janet")

# Coding Beispiel : Variablen & Platzhalter
mein_name="Janet"
Mein_Name="Elli"
alter=32
print("Hallo! Ich bin", mein_name,"und bin",alter, "alt.")

lieblingszahl=10

lieblingszahl-=2
print(lieblingszahl)


# Coding Beispiel: Input abfragen
meine_lieblingszahl=input("Welche ist deine Lieblingszahl?")
print("Deine Lieblingszahl ist", meine_lieblingszahl)


# Coding Beispiel: if-Bedingung



# Coding Beispiel: for-Schleife
for zahl in range(3):
    print(zahl)


einkaufliste=["bananen", "milch", "müsli"]
for einkauf in einkaufliste:
    print("Ich möchte noch",einkauf, "einkaufen")


preisen=[3.7, 5, 10]
for preis in preisen:
    print("Es kostet",preis, "CHF")




