import requests
import datetime


def get_connection(from_station, to_station, date, time):
    url = 'http://transport.opendata.ch/v1/connections'
    # TODO: enlist which params you can set , link to site or so
    params = {
        'from': from_station,
        'to': to_station,
        'date': date,
        'time': time
    }
    response = requests.get(url, params=params)
    return response.json()['connections']

from_station = 'Basel SBB'
to_station = 'Bern'

now = datetime.datetime.now()
date = now.date()
time = now.time()

connections = get_connection(from_station, to_station, date, time)
print(connections)
for connection in connections:
    print('Von:', connection['from']['station']['name'])
    print('Nach:', connection['to']['station']['name'])
    print('Abfahrt:', connection['from']['departure'])
    print('Ankunft:', connection['to']['arrival'])
    print('Dauer:', connection['duration'])
    print('---')
