## Schauen wir uns die API an
Das Ziel dieses Tutorials wird sein, dass wir uns alle möglichen Zugverbindungen von Stadt A nach Stadt B anzeigen lassen können. 
Dafür werden wir eine API anpingen. 

Daher schauen wir unsere API erst Mal an, damit wir wissen,
welche `query`- Parameter mitgegeben werden müssen, also Variablen die die Schnittstelle braucht, um unsere Anfrage verarbeiten zu können. 

Die Schnittstelle, die wir nutzen werden ist: 
`http://transport.opendata.ch/v1/connections`. 

Jede Schnittstelle hat immer eine Dokumentation, für unsere ist sie hier:
https://transport.opendata.ch/docs.html#connections

![alt text](api-connections.png "API /connections")

Wir sehen, dass wir sehr viele Parameter mitschicken könnten, aber für dieses Tutorial, werden wir nur `from`, `to`, `date`und `time` benutzen.

## Let's code!
Wir werden folgende Bibliotheken nutzen:
```python
import requests
import datetime
```
Mit `requests` können wir unsere HTTP-Abfragen abschicken. Mit `datetime`werden wir den aktuellen Zeitpunkt ermitteln, damit
wir sie an unsere Abfrage weitergeben können. 


1. Wir definieren eine Methode `get_connection` mit den Parametern `from_station, to_station, date, time`, 
diese Methode werden wir später aufrufen, um die möglichen Zugverbindungen abfragen zu können. Bei jeder Abfrage müssen wir dann angeben von wo wir abfahren `from_station`, wohin wir fahren `to_station`, das Datum `date` und die Uhrzeit `time`.

Der Methodenkopf sieht dann so aus: 

```def get_connection(from_station, to_station, date, time):```

2. Dann definieren wir die Variable `url` und schreiben darauf die URL, die wir abfragen wollen.
` url='http://transport.opendata.ch/v1/connections'`

3. Dann übergeben wir die Query Parameter unter `params`, damit die Abfrage erfolgen kann.

```python
params = {
        'from': from_station,
        'to': to_station,
        'date': date,
        'time': time
    }
```
Also genau die vier Parameter, die wir auch in unserem Methodenkopf angefordert haben.

Nun schicken wir die Abfrage ab, jetzt kommt die Bibliothek `requests` ins Spiel. Um eine Abfrage abzuschicken, bietet sie die `get`- Methode an, worin wir die URL und unsere Parameter mitgeben können. 
4. Die Antwort schreiben wir dann in die Variable `response` rein.

`response = requests.get(url, params=params)`


### Antwort unseres requests als JSON
Probiere die URL ```http://transport.opendata.ch/v1/connections?from=Basel&to=Bern``` in einem Browser aus, dann solltest genau so ein JSON erhalten.
Die Antwort wird als folgenden JSON dargestellt: 


```json
{
    "connections": [
        {
            "from": {
                "station": {
                    "id": "8500010",
                    "name": "Basel SBB",
                    "score": null,
                    "coordinate": {
                        "type": "WGS84",
                        "x": 47.547412,
                        "y": 7.589577
                    },
                    "distance": null
                },
                "arrival": null,
                "arrivalTimestamp": null,
                "departure": "2024-07-08T17:52:00+0200",
                "departureTimestamp": 1720453920,
                "delay": null,
                "platform": "9",
                "prognosis": {
                    "platform": null,
                    "arrival": null,
                    "departure": null,
                    "capacity1st": null,
                    "capacity2nd": null
                },
                "realtimeAvailability": null,
                "location": {
                    "id": "8500010",
                    "name": "Basel SBB",
                    "score": null,
                    "coordinate": {
                        "type": "WGS84",
                        "x": 47.547412,
                        "y": 7.589577
                    },
                    "distance": null
                }
            },
            "to": {
                "station": {
                    "id": "8507000",
                    "name": "Bern",
                    "score": null,
                    "coordinate": {
                        "type": "WGS84",
                        "x": 46.948832,
                        "y": 7.439136
                    },
                    "distance": null
                },
                "arrival": "2024-07-08T18:56:00+0200",
                "arrivalTimestamp": 1720457760,
                "departure": null,
                "departureTimestamp": null,
                "delay": null,
                "platform": "8",
                "realtimeAvailability": null,
                "location": {
                    "id": "8507000",
                    "name": "Bern",
                    "score": null,
                    "coordinate": {
                        "type": "WGS84",
                        "x": 46.948832,
                        "y": 7.439136
                    },
                    "distance": null
                }
            },
            "duration": "00d01:04:00",
            "transfers": 0,
            "service": null,
            "products": [
                "ICE"
            ],
            "sections": [
                {
                    "departure": {
                        "station": {
                            "id": "8500010",
                            "name": "Basel SBB",
                            "score": null,
                            "coordinate": {
                                "type": "WGS84",
                                "x": 47.547412,
                                "y": 7.589577
                            },
                            "distance": null
                        },
                        "arrival": null,
                        "arrivalTimestamp": null,
                        "departure": "2024-07-08T17:52:00+0200",
                        "departureTimestamp": 1720453920,
                        "delay": null,
                        "platform": "9",
                        "prognosis": {
                            "platform": null,
                            "arrival": null,
                            "departure": null,
                            "capacity1st": null,
                            "capacity2nd": null
                        },
                        "realtimeAvailability": null,
                        "location": {
                            "id": "8500010",
                            "name": "Basel SBB",
                            "score": null,
                            "coordinate": {
                                "type": "WGS84",
                                "x": 47.547412,
                                "y": 7.589577
                            },
                            "distance": null
                        }
                    }
                }
            ]
        }
    ]
}
```
Du kannst sehen, dass die Antwort sehr viele unterschiedliche Informationen zurück gibt, wie genaue GPS- Koordinaten `x` und `y`, IDs `id` der Stationen oder Zugtyp `category`.
Diese sind aktuell nicht relevant, aber du kannst sie später auch mitausgeben.
5. Zum Schluss geben wir für die Antwort die wir bekommen haben, als json konvertiert zurück. Dabei greifen wir auf den Parameter `connections`, da darunter alle Verbindungen aufgelistet sind.

```return response.json()['connections']``` 


### Mitgabe unserer Parameter: Ort & Zeit 
6. Nun haben wir die Methode definiert, mit der wir eine GET Abfrage abschicken können. Nun wollen wir auch diese Methode aufrufen, dafür brauchen wir noch die vier Parameter.
````python
from_station = 'Basel SBB'
to_station = 'Bern'
````

7. Wir wollen alle Verbindungen, die ab jetzt fahren. Wir nutzen dafür `datetime.datetime.now()` und bekommen den jetzigen Zeitpunkt.
Das schreiben wir auf die Variable `now`. Mit `now.date()` greifen wir auf das Datum und `now.time()` auf die jetzige Uhrzeit zu.
````python
now = datetime.datetime.now()
date = now.date()
time = now.time()
````

8. So! Jetzt können wir endlich unsere Funktion aufrufen!

```connections = get_connection(from_station, to_station, date, time)```

9. Nun wollen wir auch die Verbindungen auflisten, die wir erhalten haben. 
Wenn du dir das JSON von vorhin anschaust, kannst du sehen, dass alle unter dem Objekt `connections` aufgelistet sind. Diese geben wir bereits als `return`-Wert in unserer Funktion zurück.
Um die Verbindungen aufzulisten, müssen wir durch unsere Liste  `connections` iterieren.
Dann können wir ausgeben, von wo nach wo wir fahren, von welchen Bahnhöfen und die Dauer der Fahrt. Diese Informationen können wir wie folgt aus dem JSON lesen:
````python
for connection in connections:
    print('Von:', connection['from']['station']['name'])
    print('Nach:', connection['to']['station']['name'])
    print('Abfahrt:', connection['from']['departure'])
    print('Ankunft:', connection['to']['arrival'])
    print('Dauer:', connection['duration'])
    print('---')
````

Führe jetzt deinen Code aus - wie sehen die Verbindungen aus? 
Um zu überprüfen, ob es die "richtigen" Verbindungen sind, kannst du einfach auf `sbb.com` gehen und die gleichen Bahnhöfe eingeben.
Sind es die gleichen Verbindungen? 
Bei mir sieht es so aus: 
```python
Von: Basel SBB
Nach: Bern
Abfahrt: 2024-07-14T14:13:00+0200
Ankunft: 2024-07-14T15:24:00+0200
Dauer: 00d01:11:00
---
Von: Basel SBB
Nach: Bern
Abfahrt: 2024-07-14T14:20:00+0200
Ankunft: 2024-07-14T15:26:00+0200
Dauer: 00d01:06:00
---
```
Jetzt probiere noch einige andere Städte und Zeitpunkte aus! 
### Zusatzaufgabe 1:
Ganz am Anfang des Tutorials haben wir ja die Schnittstelle angeschaut und welche Parameter wir mitschicken können.
Versuche doch jetzt noch ein paar weitere Parameter zu setzen, wie sieht dann das JSON aus das zurück kommt? 

### Zusatzaufgabe 2: 
Bisher haben wir den Abfahrts- und Ankunftsbahnhof "hart codiert", heisst fest angegeben. Versuche doch diese beiden Informationen von NutzerInnen abzugreifen. 
Das kannst du mit ```input("Von wo willst du abfahren? ")``` machen. 

Tipp: Falls du noch nie mit dieser Methode gearbeitet hast, schau dir doch nochmal die Anleitung
[Taschenrechner programmieren](https://ict-scouts.edudoo.ch/slides/slide/taschenrechner-programmieren-660) an. 

### Zusatzaufgabe 3: 
Es kann sein, dass die NutzerInnen sich im Namen des Bahnhofs vertippen. Wenn du einen ungültigen Bahnhof im `request` mitschickst,
resultiert es darin, dass du keine Ergebnisse erhälst. Wie kannst du überprüfen, ob die Eingabe korrekt war? 
Kannst du den NutzerInnen auch angeben welche möglichen Bahnhöfe es gibt? Vielleicht anhand der ersten zwei bis drei Buchstaben? 

Tipp: Kannst du etwas mit dieser Api anfangen? https://transport.opendata.ch/docs.html#stations
