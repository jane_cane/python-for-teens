In diesem Tutorial wirst du lernen, wir wie mit der Bibliothek `matplotlib` einen Graphen zeichen können.
## Graphen zeichnen
Erst Mal der Import: 

`import matplotlib.pyplot as plt` 

Wir setzen einen Alias mit `as plt`, damit wir später die Methoden der Bibliothek via `plt...()` aufrufen können, statt jedes Mal den kompletten Namen schreiben zu müssen.
Praktisch oder? :) 

Den Alias nutzen wir auch direkt, indem wir die Methode `subplots()` aufrufen, diese Methode gibt uns ein Tupel, also zwei Variablen gleichzeitig zurück, die schreiben wir in unsere Variblen so auf: 
````python
fig, ax = plt.subplots()
````
Die Methode erstellt eine `figure` und eine Reihe von Subplots, welches Achsenobjekte sind. In unserem Fall werden wir unter `ax` später unsere X- und Y-Koordinaten anlegen.
Anhand dessen "plottet" er dann unseren Graphen. `fig` ist hier unser Gesamtgraph, der später angezeigt wird.

Nun setzen wir die Namen auf unseren Achsen und unserem Graphen
```python
ax.set_title('Line Graph Example')
ax.set_xlabel('X Axis')
ax.set_ylabel('Y Axis')
```

Jetzt füttern wir ihm noch ein paar Koordinaten, dafür erstellen wir zwei arrays, `x` und `y`
```python
x = [1, 2, 3, 4, 5]
y = [2, 4, 6, 8, 10]
```
Wir rufen `plot(x,y)` auf, damit er mit unseren Koordinaten einen Graüh erstellt. Anschliessend sagen wir mit `show()`, dass uns der Graph angezeigt werden soll.
```python
ax.plot(x, y)
plt.show()
```
Führe den Code aus! Dann sollte sich ein neues Fenster öffnen mit folgendem Graph:

 ![alt text](graph.png)
Sieht er bei dir auch so aus? Jetzt kannst mit den Achsennamen oder den x-y-Werten experimentieren und schauen, 
wie der Graph danach aussieht! 

## Graphen anhand einer Formel zeichnen 
Jetzt wo wir die Basics des Graphen zeichnen kennen, wollen wir bestimmte Formeln zeichnen lassen! 

### Exponentialfunktion
Die Exponentialfunktion lautet  `f(x) = e^x`, dabei ist unsere x-Koordinate eine Unbekannte und die y-Koordinate das Resultat der Funktion.
Da wir entlang der x-Achse iterieren, können wir über die Werte iterieren (x = 1,2,3...) und dadurch den y-Wert berechnen.


Zu Beginn definieren wir zwei leere Arrays `x` und `y`, die wir dann in unserer 
`for`- Schleife befüllen werden.
```python
x = []
y = []
```

Wir werden nur bis 20 iterieren. An das `x`- Array hängen wir den iterierten Wert an, für das y-Array wird es trickier.
Hier benutzen wir zwei `math`-Methoden: `pow()` und `math.e`. `pow` steht für `power`und heisst übersetut Potenz,
`math.e` können wir auf den `e`- Wert zugreifen. So können wir mit den beiden die Funktion `e^x` darstellen.

```python
for x_val in range(0, 20):
    x.append(x_val)
    y.append(math.pow(math.e, x_val))
```

Jetzt übergeben wir die Koordinaten wieder unserem subplot und zeigen uns die Figur an mit 
````python
ax.plot(x, y)
plt.show()
````

Et voilà, das Resultat sollte so aussehen: 

![alt text](e.png)


P.S.: Falls du noch mehr Graphen zeichnen möchtest, aber nicht weiss welche, dann versuch es doch mit den binomischen Formeln aus dem vorherigen Tutorial ;) Viel Spass!
