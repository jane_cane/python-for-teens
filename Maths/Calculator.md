Erstelle eine neue und leere Datei mit Namen `Taschenrechner.py`.


1. Zu Beginn geben wir dem Spielenden an, welche Optionen er hat. Wir bieten an Addition, Subtraktion,
Division und Multiplikation an. Das geben wir einfach mit `print()` aus. Den Code packen wir in die Methode
`starte_berechnung()`
```python
def starte_berechnung():
   # Frag den User, welche Berechnung er machen will: addieren, subtrahieren, dividieren oder multiplizieren
   print("Wähl eine Berechnung aus:")
   print("1. Addieren")
   print("2. Subtrahieren")
   print("3. Dividieren")
   print("4. Multiplizieren")
```
2. Jetzt nehmen wir die Eingabe des Spielenden an, das machen wir mit `input()` und die Eingabe konvertieren
wir zu einem Integer mit `int()`
```python
 eingabe = int(input("Gib die Nummer deiner Wahl ein: "))
```
3. Nun haben wir die Eingabe des Spielenden, also eine Zahl von 1-4, genau auf diese Zahlen
werden wir überprüfen. Das machen wir mit `if-elif` Schleifen.    
Auch definieren wir ein `else`, indem
wir abfangen, wenn der Spielende eine falsche Eingabe gemacht hat, also z.B. einen Buchstaben oder ein Zahl
grösser als 4.
```python
 if eingabe == 1:
    print("Du hast Addition gewählt.")
 elif eingabe == 2:
    print("Du hast Subtraktion gewählt.")
 elif eingabe == 3:
    print("Du hast Division gewählt.")
 elif eingabe == 4:
    print("Du hast Multiplikation gewählt.")
 else:
       print("Ungültige Eingabe. Bitte wähl eine Berechnung aus den Optionen 1 bis 4.")

```
4. Nun haben wir die vier Rechenoptionen, jetzt geht es ans Rechnen! 
Unter `if eingabe == 1:` nehmen wir zwei Zahlen an, die wir addieren und dann geben wir das 
Ergebnis aus. 
```python
 if eingabe == 1:
   print("Du hast Addition gewählt.")
   num1 = int(input("Gib die erste Zahl ein: "))
   num2 = int(input("Gib die zweite Zahl ein: "))
   ergebnis = num1 + num2
   print(f"Das Ergebnis ist {ergebnis}")
```
5. Nun versuche doch Mal, die nächsten Berechnungen selbst zu machen! Um zu testen, ob es richtig klappt, kannst du ja 
den Code ausführen und deinen Code mit zwei Zahlen testen! 

6. Nun haben wir alle vier Rechenoptionen programmiert, nachdem der Spielende eine Rechnung ausgeführt hat 
wollen wir nochmal fragen, ob er eine neue Rechnung starten möchte. Falls ja, rufen wir 
wieder `starte_berechnung()` auf oder wir verabschieden uns. 
```python
new_calculation = input("Möchtest du eine neue Rechnung starten? (j/n): ")
   if new_calculation.lower() == "j":
       starte_berechnung()
   else:
       print("Danke für's Benutzen meines Rechners!")
```
7. So das war es auch schon! Teste doch ob alle Rechenoptionen richtig funktionieren oder frage jemanden der deinen 
Taschenrechner ausprobieren soll! Viel Spass! 

<details>
  <summary>Klicken um Lösung anzuzeigen</summary>

```python
def starte_berechnung():
   # Frag den User, welche Berechnung er machen will: addieren, subtrahieren, dividieren oder multiplizieren
   print("Wähl eine Berechnung aus:")
   print("1. Addieren")
   print("2. Subtrahieren")
   print("3. Dividieren")
   print("4. Multiplizieren")

   # Lass den User seine Wahl treffen
   eingabe = int(input("Gib die Nummer deiner Wahl ein: "))

   # Nimm die notwendigen Eingaben und führe die Berechnung durch
   if eingabe == 1:
       print("Du hast Addition gewählt.")
       num1 = int(input("Gib die erste Zahl ein: "))
       num2 = int(input("Gib die zweite Zahl ein: "))
       ergebnis = num1 + num2
       print(f"Das Ergebnis ist {ergebnis}")
   elif eingabe == 2:
       print("Du hast Subtraktion gewählt.")
       num1 = int(input("Gib die erste Zahl ein: "))
       num2 = int(input("Gib die zweite Zahl ein: "))
       ergebnis = num1 - num2
       print(f"Das Ergebnis ist {ergebnis}")
   elif eingabe == 3:
       print("Du hast Division gewählt.")
       num1 = int(input("Gib die erste Zahl ein: "))
       num2 = int(input("Gib die zweite Zahl ein: "))
       if num2 != 0:
           ergebnis = num1 / num2
           print(f"Das Ergebnis ist {ergebnis}")
       else:
           print("Fehler: Division durch null ist nicht erlaubt.")
   elif eingabe == 4:
       print("Du hast Multiplikation gewählt.")
       num1 = int(input("Gib die erste Zahl ein: "))
       num2 = int(input("Gib die zweite Zahl ein: "))
       ergebnis = num1 * num2
       print(f"Das Ergebnis ist {ergebnis}")
   else:
       print("Ungültige Eingabe. Bitte wähl eine Berechnung aus den Optionen 1 bis 4.")

   # Frag den User, ob er eine neue Rechnung starten möchte
   new_calculation = input("Möchtest du eine neue Rechnung starten? (ja/nein): ")
   if new_calculation.lower() == "ja":
       starte_berechnung()
   else:
       print("Danke für's Benutzen meines Rechners!")

starte_berechnung()


```
</details>
