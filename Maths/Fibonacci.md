## Fibonacci
Was sind die Fibonacci-Zahlen?

Die Fibonacci-Folge ist eine unendliche Folge von Zahlen, bei der jede Zahl sich aus der Summe der beiden vorherigen Zahlen ergibt. Die ersten beiden Zahlen der Fibonacci-Folge sind 0 und 1.
D.h. die ersten fünf Zahlen sind 0,1,1,2,3.

In dieser Aufgabe werden wir eine Besonderheit im Programmieren kennen lernen, nämlich die Rekursion.
Rekursion im Programmieren heisst, eine Methode wird immer wieder und wieder aufgerufen, bis eine bestimmte
Bedingung nicht mehr erfüllt ist. Also so ähnlich wie eine `while`-Schleife, bloss als Funktion.

1. Erstelle eine neue und leere Datei mit Namen `Fibonacci.py`.
2. Wir definieren die Methode `fibonacci(n)`, dabei gibt `n` die n-te Zahl der Fibonacci-Folge an. Wir erwarten, dass `n` ein Integer ist
und geben an, dass wir auch einen Integer zuückgeben werden mit `-> int`
```python
def fibonacci(n: int) -> int:
```
3. Wir wissen die ersten zwei Zahlen, der Fibonacci-Folge sind 0 und 1. D.h. wenn der Input 1, ist der erwartete Wert 0 und wenn man die 2. Fibonacci-Zahl haben will, ist das die 1.
Genau das decken wir nun mit einem `if-elif` ab.
```python
   if n == 1:
        return 0
    elif n == 2:
        return 1
```
4. Nun stellt sich die Frage: Was ist aber dann mit der 3., 4. oder 34. Fibonacci-Zahl? Jetzt kommt ein wichtiger Teil.
Das wird nun in einem `else` behandelt, wenn die Eingabe > 2 ist, müssen wir sie aus den beiden vorherigen Zahlen addieren.   
Und jetzt kommt die Rekursion ins Spiel.    
Wir rufen unsere implementierte Methode `fibonacci(n)`mit den zwei nächst kleineren Werten auf, also `n-1` und `n-2` 
mit den Werten landet er wieder in der Methode und erst wenn die Zahl runter auf 2 bzw 1 subtrahiert wurde, 
bekommen wir einen `return`-Wert. 
```python
   else:
        return fibonacci(n - 1) + fibonacci(n - 2)
```
5. Nun haben wir eine Methode in der wir die Fibonacci-Folge berechnen können, wir definieren eine neue Methode `start_fibonacci()` in der wir den
Nutzer fragen, die wie vielte Fibonacci-Zahl wir für ihn berechnen sollen.  
Darin rufen wir `fibonacci(n)` auf und geben
dann das Ergebnis aus.
```python
  def start_fibonacci():
     n = int(input("Enter a number : "))
     result = fibonacci(n)
     print(result)
```
6. Ganz am Ende rufen wir noch `start_fibonacci()` um unser Programm zu starten. 

<details>
  <summary>Klicken um Lösung anzuzeigen</summary>

```python
def start_fibonacci():
     n = int(input("Enter a number : "))
     result = fibonacci(n)
     print(result)

def fibonacci(n:int) -> int:
    if n == 1:
        return 0
    elif n == 2:
        return 1
    else:
        return fibonacci(n - 1) + fibonacci(n - 2)

start_fibonacci()

```
</details>
