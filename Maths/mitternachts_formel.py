import cmath

def mitternachts_formel():
    """
    Berechnet die Lösungen für eine quadratische Gleichung mit der quadratischen Formel.

    Rückgabe:
    lösungen (tuple von komplex): Die Lösungen für ratische Gle.
    """

    # Frage den Nutzer nach den Effizienten
    a = int(input("Bitte geben Sie den Koeffizienten a ein: "))
    b = int(input("Bitte geben Sie den Koeffizienten b ein: "))
    c = int(input("Bitte geben Sie den Koeffizienten c ein: "))

    # Berechne die Diskriminante
    diskriminante = b*b - (4*a*c)

    # Berechne die zwei Lösungen
    loesung1 = (-b - cmath.sqrt(diskriminante)) / (2*a)
    loesung2 = (-b + cmath.sqrt(diskriminante)) / (2*a)

    # Gib die Lösungen als Tupel zurück
    return (loesung1, loesung2)

# Rufe die Funktion auf
lösungen = mitternachts_formel()

# Gib die Lösungen aus
print(f"Die Lösungen sind {lösungen[0]} und {lösungen[1]}")
