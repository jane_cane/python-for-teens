def start_fibonacci():
     n = int(input("Enter a number : "))
     result = fibonacci(n)
     print(result)

def fibonacci(n:int) -> int:
    if n <= 0:
        return "Die Eingabe sollte eine positive Ganzzahl sein."
    elif n == 1:
        return 0
    elif n == 2:
        return 1
    else:
        return fibonacci(n - 1) + fibonacci(n - 2)

start_fibonacci()
