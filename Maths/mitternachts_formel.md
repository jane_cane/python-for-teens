Kennst du noch die Mitternachtsformel? 
Falls nicht, hier ein Auffrischer:

 ![alt text](mitternacht.png)

Und wir werden diese Formel implementieren!
Wie du in der Formel siehst, gibt es drei Variablen `a,b und c`.
Diese werden wir den Nutzer abfragen.

1. Wir importieren die Bibliothek `import cmath`, damit wir später die 
Wurzelrechnung benutzen können. 
2. Definiere die Methode `mitternachts_formel`: 
```python
def mitternachts_formel():
```
3. Nun fragen wir die drei Variablen `a, b und c` ab.
```python
    a = input("Bitte geben Sie den Koeffizienten a ein: ")
    b = input("Bitte geben Sie den Koeffizienten b ein: ")
    c = input("Bitte geben Sie den Koeffizienten c ein: ")
```
4. Wir berechnen nun den Teil in der Wurzel  ![alt text](wurzel.png) 
und schreiben den Wert in `diskriminante.`

````python
 diskriminante =  b*b  - (4*a*c)
````
5. Nun haben wir in der Gleichung zwei Variationen, da wir -b +/- berechnen müssen. Es gibt also zwei Lösungen, also berechnen wir sie auch zwei Mal mit jeweils einen der Vorzeichen.

Wir lernen nun auch die Funktion `sqrt()` kennen von unserer Bibliothek `cmath`. Mit ihr können wir die Wurzelfunktion aufrufen.
Wir halten den Wert in der Wurzel unter `diskriminante`, daher brauchen wir nur noch ` cmath.sqrt(diskriminante)` aufzurufen.

````python
  loesung1 = (-b - cmath.sqrt(diskriminante)) / (2*a)
  loesung2 = (-b + cmath.sqrt(diskriminante)) / (2*a)

````
6. Da wir zwei Lösungen haben, geben wir ein Tupel, also zwei Werte zurück, das machen wir einfach mit: 
````python
 return (loesung1, loesung2)
````
7. Wenn wir sie aufrufen und die Lösung ausgeben wollen, müssen wir uns merken, dass es ein Tupel ist.
Der Zugriff ist wie auf ein Array, wir greifen daher auf den 0. und 1. Wert im Tupel zu.
````python
lösungen = mitternachts_formel()
print(f"Die Lösungen sind {lösungen[0]} und {lösungen[1]}")
````

8. Nun führe den Code aus und teste deinen Code mit unterschiedlichen Werten für a,b und c! 
